import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
public class DataBaseHelper {

    public ArrayList qwery(String splitString) throws IllegalAccessException, InvocationTargetException, IOException, InstantiationException, NoSuchMethodException, ClassNotFoundException {
        ArrayList result = null;
        String [] splittedArray = splitString.trim().split(" ");
        if (splittedArray[0].equals("select")) {
            String fileName = splitString.split(" ")[3];
            result = readAnyRecords(fileName);
            ArrayList tmpResult = new ArrayList();
            if (splittedArray.length>=5 && splittedArray[4].equals("where")) {
                String[] keyVal = splittedArray[5].split("=");
                String fieldName = keyVal[0];
                String value = keyVal[1];
                if (fieldName.equals("name")) {
                    for (Object obj: result) {
                        if (((Owner)obj).name.equals(value)) tmpResult.add(obj);
                    }
                    result = tmpResult;
                } else if (fieldName.equals("id")) {
                    int id = Integer.parseInt(value);
                    for (Object obj: result) {
                        if(obj instanceof Owner) {
                            if (((Owner) obj).id == id) tmpResult.add(obj);
                        }
                        if(obj instanceof Ticket){
                            if(((Ticket)obj).id == id) tmpResult.add(obj);
                        }
                    }
                    result = tmpResult;
                } else if (fieldName.equals("surname")) {
                    for (Object obj: result) {
                        if (((Owner)obj).surname.equals(value)) tmpResult.add(obj);
                    }
                    result = tmpResult;
                }
            }
            if(splittedArray[1].equals("name")){
                for (Object obj: result) {
                    String [] nameSplit = obj.toString().split(",");
                    tmpResult.add(nameSplit[1]);
                }
                result=tmpResult;
            }
            methodWrite(result,fileName);

        } else if (splittedArray[0].equals("update")) {
            String fileName = splitString.split(" ")[1];
            result = readAnyRecords(fileName);
            ArrayList tmpResult = new ArrayList();
            String[] keyVal = splittedArray[3].split("=");
            String fieldName = keyVal[0];
            String value = keyVal[1];
            String[] newKeyVal = splittedArray[5].split("=");
            String newValueName = newKeyVal[1];
                if (fieldName.equals("name")) {
                    for (Object obj: result) {
                        if (((Owner)obj).name.equals(value)){
                            ((Owner) obj).setName(newValueName);
                        }
                        tmpResult.add(obj);
                    }
                    result = tmpResult;
                } else if (fieldName.equals("id")) {
                    int id = Integer.parseInt(value);
                    for (Object obj: result) {
                        if (((Owner)obj).id == id){
                            ((Owner) obj).setId(Integer.parseInt(newValueName));
                        }
                        tmpResult.add(obj);
                    }
                    result = tmpResult;
                } else if (fieldName.equals("surname")) {
                    for (Object obj: result) {
                        if (((Owner)obj).surname.equals(value)){
                            ((Owner) obj).setSurname(newValueName);
                        }
                        tmpResult.add(obj);
                    }
                    result = tmpResult;
                }
            methodWrite(result,fileName);
            }
            else if (splittedArray[0].equals("delete")) {
                String fileName = splitString.split(" ")[2];
                result = readAnyRecords(fileName);
                ArrayList tmpResult = new ArrayList();
                String[] keyVal = splittedArray[4].split("=");
                String value = keyVal[1];
                for (Object obj: result) {
                    if (((Owner)obj).id == Integer.parseInt(value)){
                        continue;
                    }
                    tmpResult.add(obj);
                }
                result = tmpResult;
                methodWrite(result,fileName);
            }
            return result;
    }
    public ArrayList readAnyRecords(String fileName) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        ArrayList ownersList = new ArrayList();
        BufferedReader bufFileReader = new BufferedReader(new FileReader(fileName+".db"));
        String objectRecord = null;
        while((objectRecord = bufFileReader.readLine())!=null) {
            if (objectRecord.trim().length() > 0) {
                String[] objectRecordArr = objectRecord.split(",");
                String className = fileName.substring(0,1).toUpperCase()+fileName.substring(1,fileName.length()-1);
                Class clazz = Class.forName(className);
                Object object = clazz.newInstance();
                for (int i = 0; i < objectRecordArr.length; i++) {
                    String[] keyValue = objectRecordArr[i].split(":");

                    String fieldName = keyValue[0];
                    String fieldValue = keyValue[1];

                    if ("id".equals(fieldName)) {
                        Integer id = Integer.parseInt(fieldValue);
                        Method method = clazz.getMethod("set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1), Integer.class);
                        method.invoke(object, id);
                    }else if("ownerId".equalsIgnoreCase(fieldName)){
                        ArrayList arrayList = readAnyRecords("owners");
                        for (int j = 0; j < arrayList.size() ; j++) {
                            Owner owner = (Owner)arrayList.get(j);
                            if(owner.id == Integer.parseInt(fieldValue)){
                                Method method = clazz.getMethod("set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1,5), Owner.class);
                                method.invoke(object, owner);
                            }
                        }
                    }else {
                        Method method = clazz.getMethod("set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1), String.class);
                        method.invoke(object, fieldValue);
                    }
                }
                ownersList.add(object);
            }
        }
        return ownersList;
    }
    public void methodWrite(ArrayList arrayList,String fileName) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("newOwners.db"));
        for (int i = 0; i <arrayList.size() ; i++) {
            Object obj = arrayList.get(i);
            bufferedWriter.write(obj.toString());
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }
}
