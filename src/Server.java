import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket =  new ServerSocket(4444);
        while(true) {
            System.out.println("Wait!");
            Socket socket = serverSocket.accept();
            System.out.println("Connected!");
            MyServer myServer = new MyServer(socket,serverSocket);
            myServer.start();
        }
    }
}

class MyServer extends  Thread{

    private Socket socket;
    private ServerSocket serverSocket;

    public MyServer(Socket socket,ServerSocket serverSocket){
        this.socket = socket;
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        try{
            DataBaseHelper dataBaseHelper = new DataBaseHelper();
            BufferedReader networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter networkWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            while (true) {
                String line = networkReader.readLine();
                System.out.println("Received command: " + line);
                if (line.equals("exit")) {
                    break;
                }
                synchronized (this) {
                    ArrayList array = dataBaseHelper.qwery(line);
                    for (Object object : array) {
                        networkWriter.write(object.toString() + "\n");
                    }
                    networkWriter.write("-1" + "\n");
                }
                networkWriter.flush();
            }
            networkReader.close();
            networkWriter.close();
            serverSocket.close();
            socket.close();


        } catch (IOException io){
            io.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}