import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.util.ArrayList;

public class FrontEnd {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        System.out.println("1) select * from owners");
        System.out.println("2) select name from owners");
        System.out.println("3) select * from owners where {id=1}{name=Sulum}{surname=Habaev}");
        System.out.println("4) update owners set name=Sulum where name=Sulumbek");
        System.out.println("5) update owners set surname=Habaev where surname=Haba");
        System.out.println("6) delete from owners where id=1");
        System.out.println("7) select * from tickets where id=1");
        System.out.println("8) select * from tickets");

        Socket clientSocket = new Socket("127.0.0.1",4444);

        BufferedWriter networkWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

        BufferedReader networkReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        BufferedReader bufferedReaderString = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("qwery>");
            String str = bufferedReaderString.readLine();
            networkWriter.write(str+"\n");
            networkWriter.flush();
            String readString = "";
            print(str,null);
            while (!(readString = networkReader.readLine()).equals("-1")){
                print(str,readString.toString());
            }
            if (str.equals("exit")) {
                break;
            }
        }
        networkReader.close();
        networkWriter.close();
        clientSocket.close();
    }

    static void print(String stringFile,String stringKey) throws ClassNotFoundException {
        String [] split = stringFile.split(" ");
        String fileName = "";
        for (int i = 0; i < split.length; i++) {
            if(split[i].equals("tickets") || split[i].equals("owners")){
                fileName = split[i];
                break;
            }
        }
        if(stringKey == null) {
            String className = fileName.substring(0, 1).toUpperCase() + fileName.substring(1, fileName.length() - 1);
            Class cls = Class.forName(className);
            Field[] fields = cls.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                System.out.print(fields[i].getName() + "\t\t");
            }
            System.out.println();
        }else {
            if (split[1].equals("name")) {
                String string = stringKey.split(":")[1];
                System.out.println("\t\t" + string);

            } else if (fileName.equals("tickets")) {
                String[] string = stringKey.split(",");
                System.out.println(string[0].split(":")[1] + "\t\t" + string[1].split(":")[1] + "\t\t\t" + string[2] + " " + string[3] + " " + string[4]);

            } else if (fileName.equals("owners")) {
                String[] string = stringKey.split(",");
                System.out.println(string[0].split(":")[1] + "\t\t" + string[1].split(":")[1] + "\t\t" + string[2].split(":")[1]);

            }
        }
    }

}

