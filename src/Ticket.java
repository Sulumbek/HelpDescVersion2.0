public class Ticket {
    public int id;
    public String ticketNum;
    public Owner owner;


    public Ticket(int id, Owner owner, String ticketNum) {
        this.id = id;
        this.owner = owner;
        this.ticketNum = ticketNum;
    }
    public Ticket(){

    }

    public void setTicket(String id1) {
        int id = Integer.parseInt(id1);
        this.id = id;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public void setTicketNum(String ticketNum) {
        this.ticketNum = ticketNum;
    }

    @Override
    public String toString() {
        return "Ticket:"+ id +",ticketNum:"+ ticketNum +",Owner:"+ owner;
    }
}
