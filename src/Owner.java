public class Owner {
    public int id;
    public String name;
    public String surname;

    public Owner(int id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public Owner() {

    }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "id:" + id +",name:" + name+",surname:" + surname;
    }
}
